package com.example.android.faithconx.network;

import com.example.android.faithconx.model.ApiResult;
import com.example.android.faithconx.model.UserInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class RetrofitInstance {
    private static final String BASE_URL="https://randomuser.me/api/";
    private static PostService postService = null;
    public static PostService getPostService(){
        if(postService == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            postService = retrofit.create(PostService.class);
        }
        return postService;
    }
    public interface PostService{
        @GET(".")
        Call<ApiResult> getPostList();
        @GET("?results=100")
        Call<ApiResult> getPostLists();


    }
}
