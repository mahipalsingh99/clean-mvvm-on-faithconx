package com.example.android.faithconx;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.faithconx.databinding.ItemPostBinding;
import com.example.android.faithconx.model.UserInfo;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
    private List<UserInfo>  list;
    private Context context;
    public MyAdapter(List<UserInfo> list, Context context){
        this.list = list;
//        this.list = new ArrayList<>();
//        Post[] postArray = new Post[5];
//        for(int i=0;i<postArray.length;i++){
//            postArray[i]=new Post();
//            postArray[i].setName("demo");
//            list.add(postArray[i]);
//        }
        this.context = context;
    }

    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemPostBinding itemPostBinding = ItemPostBinding.inflate(layoutInflater,parent,false);
        return new ViewHolder(itemPostBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.ViewHolder holder, int position) {
        //UserInfo userInfo = new UserInfo(list.get(position).getName());
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(list.get(position).getProfileImage())
                .placeholder((R.drawable.ic_profile))
                .error(R.drawable.ic_launcher_background)
                .into(holder.itemPostBinding.ivProfileImage1);
        holder.itemPostBinding.setItemPost(list.get(position));
        holder.itemPostBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ItemPostBinding itemPostBinding;
        public ViewHolder(ItemPostBinding itemPostBinding) {
            super(itemPostBinding.getRoot());
            this.itemPostBinding = itemPostBinding;
        }
    }
}
