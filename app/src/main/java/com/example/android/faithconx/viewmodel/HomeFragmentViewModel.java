package com.example.android.faithconx.viewmodel;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.example.android.faithconx.model.ApiResult;
import com.example.android.faithconx.model.UserInfo;
import com.example.android.faithconx.network.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragmentViewModel {

    MutableLiveData<List<UserInfo>> userInfoList;
    public MutableLiveData<List<UserInfo>> getUserInfoList(){
        if(userInfoList == null){
            userInfoList  = new MutableLiveData<>();
        }
        return  userInfoList;
    }
    //api call
    public void getList(Context context) {

        Call<ApiResult> list = RetrofitInstance.getPostService().getPostLists();
        list.enqueue(new Callback<ApiResult>() {
            @Override
            public void onResponse(Call<ApiResult> call, Response<ApiResult> response) {
                ApiResult apiResult = response.body();

                if (apiResult != null) {
                    Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                    List<UserInfo> tempList = new ArrayList<>();
                    for (int i = 0; i < apiResult.getResults().size(); i++) {

                        tempList.add(new UserInfo(apiResult.getResults().get(i).getName().getFirst(),
                                apiResult.getResults().get(i).getPicture().getMedium(),
                                apiResult.getResults().get(i).getEmail(),
                                apiResult.getResults().get(i).getLocation().getCity(),
                                apiResult.getResults().get(i).getLocation().getCountry()));
                    }
                    userInfoList.setValue(tempList);


                }
                else{
                    Toast.makeText(context, "No Data", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ApiResult> call, Throwable t) {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }

        });
    }
}
